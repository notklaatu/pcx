#!/bin/bash
declare -a DICE_ARRAY
declare -a FUDGE_ARRAY
declare -a BONUS_ARRAY
declare -a DICE_ROLLS
declare -a FUDGE_ROLLS
ADVANTAGE=0
DISADVANTAGE=0
DIE=0
FUDGE=0
SUM=0
VERBOSE=0

function roll() {
	local d_arg="$(echo "$1" | sed 's/d/ /')"
	local num_dice="$(echo "$d_arg" | cut -d' ' -f 1)"
	local dice_side="$(echo "$d_arg" | cut -d' ' -f 2)"
	local sum=0
	
	for i in $(seq 1 "$num_dice");do
		local die="$(seq 1 "$dice_side" | shuf | head -1)"
		if [ "$VERBOSE" = 1 ];then
			printf '1d%d = [%d]\n' "$dice_side" "$die"
		fi
		sum=$(echo "$sum + $die" | bc)
	done

	DICE_ROLLS+=("$sum")
}

function froll() {
	local f_arg="$(echo "$1" | sed 's/f/ /')"
	local num_fudge="$(echo "$f_arg" | cut -d' ' -f 1)"
	local fudge_side="$(echo "$f_arg" | cut -d' ' -f 2)"
	local sum=0
	if (( fudge_side % 3 != 0 ));then
		echo "d: cannot fudge faces not devisible by three"
		exit 2
	fi

	for i in $(seq 1 "$num_fudge");do
		local fudge="$(make_fudge "$fudge_side" | shuf | head -1)"
		if [ "$VERBOSE" = 1 ];then
			printf '1f%d = [%d]\n' "$fudge_side" "$fudge"
		fi
		sum="$(echo "$sum + $fudge" | bc)"
	done

	FUDGE_ROLLS+=("$sum")
}

function add_bonus() {
	for b in ${BONUS_ARRAY[@]};do
		SUM=$(echo "$SUM $b" | bc)
	done
}

function make_fudge() {
	local fudge=""
	for i in $(seq 1 "$(expr "$1" / 3)");do
		fudge+="1 "
	done
	for i in $(seq 1 "$(expr "$1" / 3)");do
		fudge+="-1 "
	done
	for i in $(seq 1 "$(expr "$1" / 3)");do
		fudge+="0 "
	done

	echo "$fudge" | sed 's/.$//' | sed 's/ /\n/g'
}

while [ True ];do
	if [ "$1" = "--help" -o "$1" = "-h" ];then
		get_help
		exit 1
	elif [ "$1" = "--advantage" -o "$1" = "-a" ];then
		ADVANTAGE=1
		shift 1
	elif [ "$1" = "--disadvantage" -o "$1" = "-d" ]; then
		DISADVANTAGE=1
		shift 1
	elif [ "$1" = "--verbose" -o "$1" = "-v" ];then
		VERBOSE=1
		shift 1
	elif [ -n "$(echo "$1" | grep d)" ];then
		DICE_ARRAY+=("$1")
		DIE=1
		shift 1
	elif [ -n "$(echo "$1" | grep f)" ];then
		FUDGE_ARRAY+=("$1")
		FUDGE=1
		shift 1
	elif [ -n "$(echo "$1" | grep +)" -o -n "$(echo "$1" | grep -)" ];then
		BONUS_ARRAY+=("$1")
		BONUS=1
		shift 1
	elif [ -z "$1" ];then
		break
	else
		echo "d: $1 not valid input"
		exit 1
	fi
done

# Debug printf's
#printf 'Roll: %s\n' $INPUT
#printf 'Advantage: %s\n' $ADVANTAGE
#printf 'Disadvantage: %s\n' $DISADVANTAGE
#printf 'Die: %s\n' $DIE
#printf 'Fudge: %s\n' $FUDGE
#printf 'Bonus: %d\n' $BONUS
#echo "Bonus Array: ${BONUS_ARRAY[@]}"


if [ "$ADVANTAGE" = 1 -a "$DISADVANTAGE" = 1 ];then
	ADVANTAGE=0
	DISADVANTAGE=0
fi

if [ "$FUDGE" = 1 -a "$DIE" = 1 ];then
	echo "d: can't do both fudge rolls and dice rolls in the same roll"
	exit 3
fi

if [ "$DIE" = 1 ];then
	if [ "$VERBOSE" = 1 ];then
		echo "Dice rolls:"
	fi

	for dice in $DICE_ARRAY;do
		roll "$dice"

		if [ "$ADVANTAGE" = 1 ];then
			roll "$dice"
			ROLL=$(printf '%d\n' "${DICE_ROLLS[@]}" | sort -nr | head -1)
		elif [ "$DISADVANTAGE" = 1 ];then
			roll "$dice"
			ROLL=$(printf '%d\n' "${DICE_ROLLS[@]}" | sort -n | head -1)
		else
			ROLL=${DICE_ROLLS[0]}
		fi

		if [ "$VERBOSE" = 1 ];then
			printf 'Rolled: %d' "$ROLL"
		fi

		SUM="$(echo "$SUM + $ROLL" | bc)"

	done
fi

if [ "$FUDGE" = 1 ];then
	echo "Fudge rolls:"
	for fudge in $FUDGE_ARRAY;do
		froll "$fudge"

		if [ "$ADVANTAGE" = 1 ];then
			froll "$fudge"
			ROLL=$(echo "${FUDGE_ROLLS[@]}" | sort -nr | head -1)
		elif [ "$DISADVANTAGE" = 1 ];then
			froll "$fudge"
			ROLL=$(echo "${FUDGE_ROLLS[@]}" | sort -n | head -1)
		else
			ROLL=${FUDGE_ROLLS[0]}
		fi

		if [ "$VERBOSE" = 1 ];then
			printf "Rolled: $ROLL"
		fi

		SUM="$(echo "$SUM + $ROLL" | bc)"

	done
fi

if [ "$BONUS" = 1 ];then
	for bonus in $BONUS_ARRAY;do
		add_bonus "$bonus"
		if (( $bonus < 0 )) && [ "$VERBOSE" = 1 ];then
			printf '%d' "$bonus"
		elif [ "$VERBOSE" = 1 ];then
			printf '+%d' "$bonus"
		fi
	done
fi

if [ "$VERBOSE" = 1 ];then
	echo
	printf "Total:"
fi

if [ "$ADVANTAGE" = 1 ];then
	printf '\e[1;32m'
elif [ "$DISADVANTAGE" = 1 ];then
	printf '\e[1;31m'
fi

printf '%d\n' "$SUM"

if [ "$ADVANTAGE" = 1 -o "$DISADVANTAGE" = 1 ];then
	printf '\e[0m'
fi
