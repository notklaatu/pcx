usage: v
export CHAR\_SHEET "/path/to/char.xml"
v char.xpath.with.dots.instead.of.slashes

usage: d
d \[options\] \[number of dice\]d\[number of sides\] \[\+|\-\ bonus\]
or
d \[options\] \[number of fudge dice\]f\[number of sides equal to 0 % 3\] \[+|\- bonus\]

available options:
--advantage | -a: Roll twice and take the higher result (highlighted green)
--disadvantage | -d: Roll twice and take the lower result (highlighted red)

examples: v
export CHAR\_SHEET="~/Documents/char.xml"
v char.name
=> Bob

examples: d
d 3d6
=> Rolls 3d6
